#!/usr/bin/python3
import re
import sys

def ste(s):
    return s

def st(s):
    return ste(s) + "&"

def cl(s):
    if re.match("^[ -]*$", s):
    	return st(s)
    return "%.2f&" % (float(s) / 25)


copy = 0
f = sys.stdin.readlines()
for l in f:
    if re.match("^TABLE$", l):
        copy = 1
        continue
    if re.match("^ENDTABLE$", l):
        copy = 0
        continue
    if copy == 1:
        copy = 2
        continue
    if copy != 2:
        continue
    s = l.split("|")
    s = s[1:]
    print(st(s[0])+st(s[1])+cl(s[2])+cl(s[3])+cl(s[4])+cl(s[5])+ste(s[6])+"\\\\ \hline")

