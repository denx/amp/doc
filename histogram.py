#!/usr/bin/python
# python3: ImportError: No module named 'mpl_toolkits'

import math

# FIXME: verify the numbers are in usec, not in ticks

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable
import matplotlib.pyplot as plt

# Tex support is at
# http://matplotlib.org/users/usetex.html

# Generate histogram limits for C code.
class Histogram:
    def __init__(m):
        limits = []
        for i in range(100):
            v = math.exp(3.0+0.072*i)
            v = int(v)
            limits += [v]
            print "%d," % v, 
        print()
        m.limits = limits

class Figure:
    def __init__(m, data, name = "unnamed"):
        m.data = data
        m.name = name

class Painter:
    def __init__(m):
        h = Histogram()
        m.limits = h.limits
        
    def paint(m, ax, figure, opt, last):
        data = figure.data
         
        sum = data[-1]
        
        dy = data[:-1]
        sum2 = reduce(lambda x,y: x+y, dy)
        assert sum == sum2

        
        dx = m.limits
        ex = []
        ey = []
        for i in range(len(dx)):
            if dy[i] > 0:
                ex += [dx[i]/25.]
                ey += [(dy[i]*100.0)/sum]

        maximum = reduce(lambda x,y: max(x,y), ey)
        print("maximum ", maximum)

        if opt == 1:
            ax.axis([0, 105, -maximum/10, maximum * 1.3])
        elif opt == 3:
            ax.axis([0, 2.2, -maximum/10, 119])
        else:
            ax.axis([0.45, 120, 1./sum, maximum * 10000])

        if opt == 0:
            ax.set_xscale('log')
            #ax.set_yscale('symlog', basey=10000, linthreshy=100, linscaley=1)
            ax.set_yscale('log')

        if opt == 1:
            pass
            #ax.annotate(figure.name, xy=(80, 1./sum), xytext=(20, maximum/2.), )
        elif opt == 3:
            pass
            #ax.annotate(figure.name, xy=(0.5, 1./sum), xytext=(0.5, 100), )            
        else:
            ax.annotate(figure.name, xy=(80, 1./sum), xytext=(1, maximum*10), )

        yt = []
        if maximum > 0.3:
            yt = [0.5]
#        elif maximum > 0.4:
#            yt = [0.25]
#        elif maximum > 0.1:
#            yt = [0.1]
        else:
            yt = [0.1]

        if opt == 1:
            #ax.set_yticks([0] + yt)
            pass
        elif opt == 0:
            ax.set_yticks([0.001, 1])
                

        if opt == 2:
            ax.set_xticks([ 0.5, 1, 1.5, 2, 2.5, 3, 3.5 ])
        elif opt == 1:
            ax.set_xticks([ 2, 10, 50, 100 ])
        else:
            pass
            # ax.set_xticks([ 1, 2 ])            

        plt.ylabel('%')
        if last:
            plt.xlabel(u'\u00B5sec')
        else:
            for label in ax.get_xticklabels():
                label.set_visible(False)
                            
        plotx, ploty = ex, ey
        
        ax.set_title(figure.name)
        ax.titlesize = 'small'
        ax.title.set_visible(True)
        ax.grid(True)

        lines = ax.plot(plotx, ploty, 'bo', ms=4, lw=1, ls='-')
        #ax.setp(lines, color='b', linewidth=1.0)

        #plt.ylabel('relative frequency')

    def paint_percentiles(m, ax, figure, opt, last):
        data = figure.data
         
        sum = data[-1]
        dy = data[:-1]
        sum2 = reduce(lambda x,y: x+y, dy)
        assert sum == sum2
        
        dx = m.limits
        ex = []
        ey = []
        cumulative = 0
        for i in range(len(dx)):
            if dy[i] > 0:
                ex += [dx[i]/25.]
                cumulative += dy[i]
                ey += [(sum-cumulative-0.0)/sum]

        ax.axis([0, 105, -0.000001, 1.1])
        ax.set_yscale('symlog', basey=10, linthreshy=0.01, linscaley=1)
        #ax.set_yscale('log')

        ax.annotate(figure.name, xy=(80, 1./sum), xytext=(20, 1/2.), )

        yt = [ 0.5, 0.1, 0.05, 0.01, 0.001 ]
        ax.set_yticks([0] + yt)

        ax.set_xticks([ 2, 10, 50, 100 ])


#        for label in ax.get_yticklabels():
#            label.set_visible(False)
        if not last:
            for label in ax.get_xticklabels():
                label.set_visible(False)
                            
        plotx, ploty = ex, ey
        
        #ax.set_title("test title")
        ax.title.set_visible(False)
        ax.grid(True)


        lines = ax.plot(plotx, ploty, 'bo', ms=4, lw=1, ls='-')
        
        #ax.setp(lines, color='b', linewidth=1.0)
        plt.xlabel(u'\u00B5sec')
        

    def test_tests(m):
        tests = []
        # Linux, find over nfsroot running in some tests.
        tests += [ Figure([0,0,0,0,0,4,67,3758,4755,715,151,134,204,109,25,17,14,8,9,7,7,4,0,4,0,4,1,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10000]) ]
        tests += [ Figure([0, 0, 0, 1, 0, 86, 862, 46582, 49890, 6490, 1456, 1118, 1393, 1209, 302, 155, 123, 92, 77, 39, 34, 15, 4, 7, 8, 8, 6, 6, 7, 10, 9, 10, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 110000]) ]
        tests += [ Figure([0, 0, 0, 1, 0, 95, 1012, 54888, 59134, 8178, 2022, 1687, 2067, 2184, 758, 518, 591, 795, 1151, 1204, 1087, 769, 325, 261, 193, 147, 129, 131, 100, 103, 79, 73, 67, 50, 46, 45, 35, 29, 19, 18, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 140000])]
        tests += [ Figure([0, 0, 0, 1, 0, 95, 1012, 54888, 59134, 8179, 2031, 1710, 2123, 2315, 953, 781, 931, 1590, 2831, 3733, 5115, 5224, 2658, 1535, 647, 338, 266, 320, 287, 347, 257, 209, 134, 90, 74, 54, 39, 30, 20, 18, 6, 4, 1, 4, 6, 2, 3, 0, 0, 0, 1, 0, 0, 1, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 160000])]
        tests += [ Figure([0, 0, 0, 1, 1, 95, 1015, 54958, 59232, 8411, 3613, 7522, 16483, 39497, 55191, 70255, 96015, 202883, 438159, 670456, 1081023, 1264750, 684018, 392498, 133125, 58508, 43420, 45955, 49074, 59849, 52812, 40144, 21627, 11969, 7497, 2095, 811, 720, 650, 684, 617, 633, 658, 710, 709, 548, 349, 173, 114, 70, 73, 64, 62, 70, 54, 49, 9, 12, 9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5680000])]

        tests += [ Figure( [0, 0, 0, 2, 30, 14416, 188201, 9480085, 10621142, 1799943, 476436, 391553, 444182, 219561, 90582, 98119, 124146, 227542, 453681, 680509, 1087941, 1268754, 685728, 393906, 134102, 59376, 44414, 47063, 50103, 61063, 54005, 41658, 21924, 12096, 7580, 2171, 871, 754, 660, 696, 625, 636, 661, 711, 712, 550, 349, 173, 114, 71, 73, 64, 62, 70, 54, 49, 9, 12, 9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29290000] ) ]

        # AMP Linux + sparrow, mtbench.
        tests += [ Figure( [0, 0, 743, 7511, 1711, 14, 5, 3, 3, 5, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10000]  ) ]
        tests += [ Figure( [0, 0, 1526, 15113, 3300, 28, 13, 4, 3, 7, 5, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000] ) ]
        tests += [ Figure([0, 0, 3162, 30508, 6174, 69, 33, 13, 8, 25, 6, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40000] ) ]

        # Baremetal Sparrow, polling

        tests += [ Figure( [8883, 1116, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10000] ) ]
        tests += [ Figure( [17847, 2152, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000] ) ]
        m.tests = tests

    def real_tests(m):

        tests = []
        # Linux, find over nfsroot running in some tests.
        tests += [ Figure([17847, 2152, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 poll latency, CPU #1 reset") ]
        tests += [ Figure([0, 19994, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 interrupt latency, CPU #1 reset") ]
        tests += [ Figure([13969, 1968, 4060, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 AMP (idle) , CPU #1 poll latency") ]
        tests += [ Figure([0, 0, 5680, 12722, 1595, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 AMP (idle), CPU #1 interrupt latency") ]
        tests += [ Figure([13797, 2038, 4036, 42, 43, 31, 5, 4, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 AMP throughput, CPU #1 poll latency") ]
        tests += [ Figure([0, 0, 2145, 11118, 6497, 139, 71, 21, 3, 3, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 AMP throughput, CPU #1 interrupt latency") ]
        tests += [ Figure([0, 0, 2018, 10734, 7229, 2, 1, 1, 0, 1, 3, 4, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 AMP find /, CPU #1 interrupt latency") ]

        tests += [ Figure([0, 0, 0, 0, 0, 0, 906, 6908, 7083, 2939, 627, 281, 182, 148, 136, 104, 85, 108, 116, 111, 85, 52, 32, 6, 10, 9, 17, 10, 9, 9, 17, 6, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 UP latency, CPU #1 reset") ]
        tests += [ Figure([0, 0, 0, 0, 0, 0, 51, 302, 357, 176, 70, 53, 41, 54, 71, 113, 143, 197, 218, 238, 262, 272, 248, 392, 441, 514, 825, 1102, 1408, 2439, 3103, 3108, 2156, 723, 364, 189, 136, 73, 20, 30, 17, 14, 25, 8, 5, 8, 2, 2, 4, 1, 5, 1, 3, 5, 3, 5, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 UP latency/throughput, CPU #1 reset") ]
        tests += [ Figure([0, 0, 0, 0, 0, 3, 24, 67, 90, 544, 1731, 3635, 4361, 4286, 2184, 902, 358, 220, 174, 172, 184, 203, 217, 279, 154, 59, 23, 31, 16, 17, 19, 19, 12, 5, 3, 4, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 SMP latency") ]
        tests += [ Figure([0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 4, 4, 15, 16, 42, 60, 107, 98, 100, 115, 54, 124, 138, 145, 157, 217, 363, 575, 836, 1453, 2058, 1640, 801, 427, 167, 60, 30, 36, 35, 23, 14, 16, 15, 5, 7, 4, 6, 3, 2, 5, 1, 6, 0, 2, 7, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10000], "CPU #0 SMP latency/throughput") ]
        tests += [ Figure([0, 0, 0, 0, 0, 5, 39, 379, 873, 2332, 2847, 2450, 1581, 1138, 532, 279, 273, 415, 391, 304, 285, 294, 149, 113, 73, 108, 654, 342, 319, 293, 308, 401, 615, 531, 323, 534, 478, 207, 87, 26, 2, 0, 0, 0, 2, 3, 2, 3, 1, 2, 1, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPU #0 SMP throughput, CPU #1 SMP latency (CPU affinity)") ]

        tests += [ Figure([0, 0, 0, 0, 0, 0, 9, 37, 37, 48, 45, 138, 626, 2631, 4793, 4760, 3195, 1721, 647, 267, 242, 223, 197, 181, 80, 33, 23, 19, 7, 5, 10, 7, 5, 3, 2, 2, 3, 0, 0, 1, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPUs SMP latency") ]
        
        tests += [ Figure([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 1, 4, 23, 48, 49, 52, 146, 310, 378, 379, 346, 280, 284, 320, 276, 348, 400, 519, 749, 883, 1193, 1551, 1755, 1675, 1440, 1565, 1478, 1202, 814, 464, 206, 138, 95, 53, 84, 67, 60, 68, 37, 48, 38, 30, 30, 27, 9, 7, 10, 7, 14, 9, 11, 9, 5, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20000], "CPUs SMP latency/throughput") ]
        
        m.tests = tests

    def one_figure(m, name, opt):
        for i in range(len(m.tests)):
            t = m.tests[i]
            ax = plt.subplot(14, 1, i+1)
            m.paint(ax, t, opt, i+1 == len(m.tests))

        plt.savefig(name)

    def one_figure_amp(m, opt):
        for i in range(0, 7):
            t = m.tests[i]
            ax = plt.subplot(7, 1, i+1)
            m.paint(ax, t, opt, i+1 == 7)
        plt.savefig("big_amp")
        
    def one_figure_linux(m, opt):
        for i in range(7, len(m.tests)):
            t = m.tests[i]
            ax = plt.subplot(7, 1, i-6)
            m.paint(ax, t, opt, i+1 == len(m.tests))
        plt.savefig("big_linux")

    def one_figure_percentiles(m, opt):
        for i in range(7, len(m.tests)):
            t = m.tests[i]
            ax = plt.subplot(7, 1, i-6)
            m.paint_percentiles(ax, t, opt, i+1 == len(m.tests))
        plt.savefig("big_percentiles")

    def run(m):
        m.real_tests()
        plt.rcParams.update({'axes.labelsize': 'small'})                
        plt.rcParams.update({'axes.titlesize': 'small'})        
        #plt.figure(1, figsize=(8,10))
        #m.one_figure("linear", 1)
        plt.figure(2, figsize=(8,10))
        m.one_figure_amp(3)        
        plt.figure(3, figsize=(8,10))
        m.one_figure_linux(1)        
        #plt.figure(4, figsize=(8,10))
        #m.one_figure_percentiles(1)        
        plt.show()
            
        #make_axes_area_auto_adjustable(ax2, pad=0.1, use_axes=ax)

h = Histogram()
p = Painter()
p.run()
    

