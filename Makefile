amp-whitepaper.pdf: amp-whitepaper.tex table.tex big_amp.png
	pdflatex amp-whitepaper.tex

big_amp.png: histogram.py
	./histogram.py

table.tex: paper.org table.py
	./table.py < paper.org > table.tex

