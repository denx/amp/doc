\documentclass[a4paper]{article}
\newcommand{\ucos}{$\mu$c/OS-II}
\newcommand{\usec}{$\mu$sec}
\newcommand{\socfpga}{Altera SoCFPGA}
\newcommand{\joke}[1]{}
\newcommand{\code}{\tt}
\newcommand{\isquaredc}{I$^2$C}
\newcommand{\maxten}{max$_{10}$}
\title{Asymmetric Multiprocessing --- When to use, and when not}
\author{Pavel Machek, DENX Software Engineering GmbH}
\date{May 2016}
\usepackage{hyperref}
\usepackage{graphicx}
\begin{document}
\maketitle
\section {Introduction}

% MAYBE? Avoid term Asymmetric Multiprocessing as it means 6 things in 6 papers?

This paper explores the system setup possibilities on
multi-core systems. The most common configuration is Symmetric Multiprocessing (SMP),
where one operating system runs on all CPU
cores. When a general-purpose operating system (such as Linux) is
used, this configuration gives great performance for computation
tasks. But in an embedded system, raw performance is often not the most
important metric; often, real-time behaviour is more important and usually
the system needs to respond within given latency constraints for a response
to be useful. [More frames per second for a car navigation is nice,
but it is critical that ignition fires when it should.\joke{Please
don't go designing cars.}]

Asymmetric Multiprocessing (AMP) is a configuration where each core may
run a different task and different operating system. In some cases, this can
be more suitable than the usual SMP configuration. Note that in other
papers AMP sometimes refers to systems with different processors running
with shared memory, such as ARM big.LITTLE technology, NUMA systems, and
systems with DSPs. That is not focus of this paper.

This paper will focus on Linux \cite{linux} on one core,
but the results should be applicable
to other general-purpose operating systems (such as FreeBSD
\cite{freebsd} or Minix \cite{minix}). Open-source U-Boot
\cite{u-boot} will be used as a boot-loader, but the boot-loader does
not have a great effect on the system design. The implementation was done on
the SoCFPGA Cyclone V SoC board (\cite{board}) from Altera, now part of Intel, and the
results should be directly usable on all \socfpga{} systems.

FPGAs present great flexibility: it is possible to perform precise latency measurement
using custom FPGA designs. It is also possible to implement
time-critical tasks directly in the FPGA fabric, or in a processor synthesized in the FPGA fabric. Those options will be briefly mentioned later (\nameref{sec:fpga}), but they are not
the focus of this paper. These results are directly applicable to other
dual-core ARMv7 systems, and should be similar to the numbers on other
high-performance multi-core systems.

\section {Possible use cases for AMP}

% Summary of possible AMP advantages, like reduced latencies and
% improved separation between critical and non-critical tasks.

Mainline Linux is well-understood and easy to work with, but does not provide
real-time guarantees (as of version 4.4). Real-time operating systems on the other hand
do provide real-time guarantees, but programming for them is harder and it is hard for
a real-time operating system to match the Linux performance and availability
of software and other libraries.

% It would make some sense to mention RTLinux here? Realtime _and_
% easy to program for. OTOH its latencies will be significantly
% higher, and we don't yet have data to explain that here.

If all the software running on a device needs real-time guarantees, then
using a real-time operating system is the best choice. But this is often not
the case. A lot of systems have a relatively small real-time part,
and a big non time-critical and mostly separate part, which handles
for example the configuration, the user interface and the network communication.
[A wind turbine 
control system with a web server for configuration.] In such cases,
a solution where non-critical parts are running on Linux, and the real-time
parts are running on a real-time OS such as \ucos \cite{ucos} (or on
bare-metal, without an operating system) might be easier to develop.

It is also possible to implement the real-time parts on a dedicated hardware
(e.g. CPU, PIC, micro-controller). This provides the best separation,
but also increases the hardware
complexity (and often cost).

The solution being explored here is Asymmetric Multiprocessing (AMP), where
one CPU runs Linux and the second CPU runs
a different operating system. This provides some separation and does not
need any additional hardware. The separation can be increased when
hardware features (such as TrustZone \cite{trustzone}) are used to
enforce it.

In some safety-critical contexts, there's an additional issue of
certification. Certification requirements differ greatly from industry
to industry, but certified software is very hard to develop (the effort
for certification can be many times greater than the effort for writing the
software in the first place) and also expensive to license. One
method of separating critical and non-critical components is a
simple ``hypervisor'', that can be certified relatively
easily\joke{(only taking few hundred man-years)}.

The software complexity of
critical parts in AMP solutions can be significantly lower than even
simple hypervisors, making certification efforts easier.

AMP is not a solution for every problem, and it can carry some
significant costs.

If the real-time part is so complex that a bare-metal application is
not feasible, it will mean two operating systems running in the
project. With potentially different licenses, different interfaces and
coding styles, different build and debug tools. \joke{Oh and good luck
debugging that.}  Debugging two operating systems at the same time
can be tricky, as it may not be clear which system is causing
the problems under investigation.

High-performance SMP-capable cores are complex (with multiple levels
of caches, instruction scheduling, speculative execution, memory
protection, TLBs, cache coherency protocols etc). So debugging
performance problems can be difficult, and getting hardware and software
certifications will be hard.

\section {Hardware support for AMP and SMP}

% Discussion of what needs to be configured in hardware to reproduce the
% results, and what other possibilities are available.

In the AMP case, SoC needs to be split into two parts: one
controlled by Linux, one by the real-time system.

For many hardware devices, this separation is easy: either Linux will
control the device, or the real-time system will, but not both.

These devices include serial ports, Ethernet ports, timers, GPIO, SPI
and \isquaredc{} controllers. (Fortunately \socfpga{} has 4 global hardware timers, and Linux
only needs two.)
One way to make this separation is to disable the devices that should
not be used in the device tree. (And do the same for the
real-time OS, if it uses the device tree).

The situation will become more complicated with devices that need to be shared
between the two operating systems. It may be easiest to design
hardware in such a way, that this does not happen. If that is not
possible, the real-time parts should get by default the control over the hardware, with
Linux requesting hardware changes by sending messages to the real-time
part. (Yes, this will complicate the design somewhat.\joke{So avoid it at all costs.})

An example might be a GPIO controller. If just one operating system needs access to all the
GPIO ports on the controller, things are easy. It may be possible to
design hardware in a way that GPIOs are suitably grouped, for example by
creating additional GPIO controllers in the FPGA fabric (or by
attaching additional GPIO controllers, for example to the \isquaredc{} bus).

Some hardware will need special handling.

\subsection {Memory}

Both operating systems will need access to the system memory. Each
operating system should get a separate memory partition, aligned to
at least the page size, so that the MMU and/or TrustZone can be used to protect
``the other'' operating system from accidental damage.

Another issue is the memory bandwidth; non-trivial operating systems will
need access to the main memory for its operation. Hardware mechanisms should be used
to either give priority to the real-time parts, or at least to
guarantee a pre-set percentage of memory bandwidth for the real-time use.

\subsection {Cache}

Modern CPUs need cache memories to function efficiently. Again,
hardware should be set up in a way that some percentage of the cache memory is
guaranteed to be available for the real-time operating system.

There's only one cache controller on \socfpga. So 
TrustZone is used to disallow cache configuration access from Linux, and use SMC calls from
Linux to communicate requests to the secure mode.

\subsection {Interrupt controller}

The interrupt latency is critical for real-time systems, therefore
real-time interrupts should be routed to the CPU running the real-time
operating system. If possible (and the ARMv7 GIC in the \socfpga{} can do that),
non-critical interrupts should be routed directly to the Linux
partition. (Otherwise the real-time operating system would need to handle
interrupts itself, and send the message to the Linux partition for each hardware interrupt.)

\subsection {Hardware support for operating-system sandboxing}

ARMv7 cores expose information about ``who'' performs the hardware
access to the outside devices. The access can be either ``secure'' or
``non-secure''. They also allow the operating system to be executed either
inside the ``non-secure'' or inside the ``secure'' world. Only minor modifications
are needed for the operating system.

Other CPU architectures often have similar kind of support,
called ``virtualization'' or ``system management mode'', but ARM
appears to be unique in its simplicity here.

On \socfpga, Linux usually runs in the ``secure world'', having full
access to all the hardware resources, and ``non-secure world'' is not used. Most resources on \socfpga{} have
the option to either allow or deny access from the ``non-secure world''. This
allows us to implement a hardware protection between Linux and the real-time
operating system very easily.

This is good enough to prevent accidents. To reliably separate the
``secure'' and ``non-secure'' worlds, additional analysis should
be done; in particular, any devices capable of overwriting
arbitrary memory using DMA accesses need to be locked away from the
``non-secure'' world.

For this project, Linux was moved into the ``non-secure'' world. Linux
still gets full access to all the hardware, except hardware used by
the critical task. Critical task uses level 2 cache controller,
one timer, and GPIO controller.

% http://infocenter.arm.com/help/topic/com.arm.doc.prd29-genc-009492c/PRD29-GENC-009492C_trustzone_security_whitepaper.pdf

% Communication options.

\section {Configuration of U-Boot and Linux}

% Description of steps to reproduce the results, including software
% versions used and necessary patches.

For an easy development, system is booted over the network. The Linux kernel is loaded using the tftp command, then proceeds to boot from
a NFS root filesystem.

% The root filesystem should not really matter for our tests. I'm using
% armv7a-hf variant of rootfs-sato-sdk filesystem, from eldk-5.4. Serial
% port is used for console access, for logging of kernel messages, and
% for output of the bare-metal application.

The Linux 4.4 kernel is used, with patches to support AMP \cite{amp-linux},
and with additional patches to measure interrupt latency. The same source
code is used for all the tests, but the configuration options need to
differ according to the configuration (UP, SMP and AMP), and whether latency
measurement is enabled ({\code CONFIG\_RACE}). The template configuration files
are {\code config.up}, {\code config.smp} and {\code config.amp}.

U-Boot 2016.01 is used as the boot loader. Mainline U-Boot can be used
to start the system. A patched U-Boot version \cite{amp-u-boot} contains
the default environment suitable for running the AMP tests. Those changes are
in the {\code include/configs/socfpga\_cyclone5\_socdk.h} file.

Sparrow \cite{amp-sparrow} is a glue layer between U-Boot and the Linux kernel that can configure
AMP, configure TrustZone, and provides runtime services for kernel. Its libraries are 
also used to implement a bare-metal latency-testing
application.

The Linux command line is shared in most of the configurations. A NFS root configuration is used. The memory is limited in all the configurations to
reserve enough space for Sparrow. For the SMP test with interrupt
affinity, the {\code isolcpus} option is used, as described below.

The {\code run linux} command in U-Boot can be used to run Linux in a traditional way
-- that is useful for Linux in UP and SMP configurations. It loads
zImage and dtb to fixed addresses, then starts Linux.

{\code run metal} can be used to run Sparrow, without starting Linux on the other
CPU. It loads the Sparrow code to a low address, and then jumps to it using
the ``go'' command.

{\code run ampl} can be used to run AMP configurations. Linux uImage, dtb, and
Sparrow are loaded to fixed addresses. Then, U-Boot launches Sparrow
using the ``bootm'' command.

Sparrow is used to set up AMP configurations. Sparrow contains
several modules according to different configurations:
``baremetal'' for latency tests without Linux, and ``linux\_baremetal''
for AMP tests.

The file {\code src/race/timing.c} contains the interrupt timing code. It is designed
in such a way that it can be compiled into Sparrow or into
Linux. {\code USE\_IRQ} define can be used to select polling and
interrupt-driven configurations for Sparrow.


\section {Performance / throughput and latency results}

The tests will measure the interrupt latency and the system
throughput. Because AMP is mostly suitable for non-complex tasks,
a simple "latency" task, that runs completely in the
interrupt context, will be used. Therefore results for interrupt latency will be
quite low, only counting time from the hardware event to the start of
the device-specific part of the interrupt handler. A similar approach
can be found in the timings of the ``kernel'' variants in ``How fast
is fast enough?'' paper \cite{xenomai-preempt-paper}.

\subsection {Measuring latency}

The hardware timers present in the \socfpga{} are used to measure the interrupt
latency. Besides the local timer in the Cortex-A9 (arm,cortex-a9-twd-timer @
fffec600), the SoC has 4 hardware timers. Linux usually uses timers 0 and 1
(snps,dw-apb-timer-sp @ ffc08000, @ ffc09000), so the remaining timers can be used for performance measurements.

Timers are configured to ``ONESHOT'' mode. In this mode they count down
from a selected value, generate an interrupt when they reach zero, and then
continue counting into negative numbers. The interrupt handler reads
the timer value. After making the number positive, it is the
interrupt latency.  It then computes the minimum, maximum, average, number of events in each latency bin (and floating
average, useful for debugging and sanity checks), and prints the
results (not too frequently). It then
programs the next timer event to occur a pseudo-random number of ticks in the future.

% Notice that we expect latency-sensitive task to be very simple -- that
% is simple enough to be done completely in interrupt context. Thus we
% measure latency between hardware event and start of processing in the
% interrupt handler. (Other people may be interested in latency between
% hardware event and start of user-land processing, and their numbers
% will differ.)

% The maximum latency for real-time operating systems or bare-metal
% applications can be expected to stay in the range of the measured
% values. For the general-purpose operating system, there is a big
% variation in the maximum latencies, and with different workload, higher latencies than our maximum might be reached.

\subsection {Measuring throughput}

To measure the throughput of the system, a multi-threaded task is needed, so
that it can use all the available CPU cores. A pure CPU-bound task such
as LM-bench is not be suitable, as it does not cause a significant
amount of interrupts, and latencies measured by the latency task
would be lower than those expected in more realistic scenarios.

% ### General comment:
%I'm pretty sure that I've mentioned this before. LM-bench is definitely
%not a 'pure CPU-bound task'. It includes many test benchmark and
%test cases (IO, memory, networking etc).

% http://www.bitmover.com/lmbench/lmbench-summary

Throughput task also needs to do a significant number of hardware accesses;
that way, if the load from the throughput task on the busses causes
a significant slowdown on the other CPU, there will be a chance to detect
it. It is best not to use resources outside of the development board for
testing, so that the tests are easy to reproduce.

Thus test consists of reading 400kB from on-board NOR flash
(discarding the results) and compression 50kB of random data, using
the bzip2 compressor, in a loop. Reading the data will load the
hardware, while data compression will load a CPU and serve
as a benchmark. 6 threads are used to load all the CPU cores. The number
of loop completions in 10 seconds is used as the ``mtbench'' score.

\subsection {Affinity on SMP Linux}

On a SMP Linux system, usually the kernel decides on which CPU each
task is executed. But for special applications, it is possible to
control the placement of interrupts and tasks manually. Using
{\code isolcpus=1} on the kernel command line forces Linux not to schedule
tasks on CPU \#1 automatically. Interrupt used for latency
measurement is manually set to CPU \#1. Latencies are measured on timer3, interrupt
``GIC 202'', which corresponds to the Linux IRQ \#18, so the command is {\code echo
2 $>$ /proc/irq/18/smp\_affinity}.

\subsection {Benchmark environment}

In the Linux case, the latency measurements are started after boot.

In the case of a non-real-time operating system, the maximum latency
is not bounded. That means that the maximum latency is $\infty$, and
the maximum observed latency varies greatly with
time. With enough patience, it would be possible to obtain values bigger
than those presented below. This paper did not try to find the worst-case
load. For Linux tests, it is probable that ``hackbench'' and
``dohell'' tests would result in greater task-switching latencies (but
this paper is about interrupt latencies, and those tests are not
designed to interfere with real-time tasks on second core). For AMP
tests, there are tests that affect the hardware more than mtbench, but
are not easily repeatable. One such example is ``find /'' over a NFS
mount (and the results are included below for comparison), but it is
probable that even higher latencies are possible with different
hardware loads.

\subsection {Results}

In the table 1, ``poll'' is the bare-metal application that polls
for events, ``interrupt'' is the bare-metal application that uses
interrupts, ``UP'', ``AMP'' and ``SMP'' is Linux used in Uniprocessor,
Asymmetrical Multiprocessing and Symmetrical Multiprocessing
configurations respectively. ``lat.'' is the latency measuring task,
``thr.'' is the mtbench, throughput-measuring task.

The table lists minimum, average and maximum latencies. In
the bare-metal cases, the first measurement is cache-cold,
and thus often higher than all other latencies. To preserve information about
this, \maxten is listed, too -- latency of all but the first 10
measurements. First cache-cold measurements do not affect \maxten.

Figures 1 and 2 present detailed relative frequencies of latencies for
selected configurations.

\begin{table*}
\begin{center}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
%\multicolumn{7}{|c|}{Results, 20000 interrupts or cca 6 minutes, latencies in \usec}\\ \hline
CPU \#0&CPU \#1&min&avg&\maxten&max&mtbench\\
%&&&&&&\\
\hline
 poll lat.     & reset         &0.48&0.64&0.80&1.00& -            \\ \hline
 interrupt lat.& reset         &0.80&0.80&0.84&1.76& -            \\ \hline
 AMP idle      & poll lat.     &0.52&0.70&0.92&1.08& -            \\ \hline
 AMP idle      & interrupt lat.&0.88&0.91&1.16&1.88& -            \\ \hline
 AMP thr.      & poll lat.     &0.52&0.70&1.40&1.40& (27)..30..32 \\ \hline
 AMP thr.      & interrupt lat.&0.88&0.93&1.60&1.88& (26)..30..32 \\ \hline
 AMP find /    & interrupt lat.&0.88&0.93&1.80&1.88& -            \\ \hline
 UP lat.       & reset         &1.12&1.40&8.84&8.84& -            \\ \hline
 UP lat./thr.  & reset         &1.12&6.10&48.48&48.48& 30..35       \\ \hline
 UP thr.       & reset         &   - &     - &    - &     - & (30)..31..35 \\ \hline
 SMP lat.      & -             &1.08&2.01&14.52&14.52& -            \\ \hline
 SMP lat./thr. & -             &1.36&7.45&50.44&50.44& (32)..35..38 \\ \hline
 SMP thr.      & -             &   - &     - &    - &     - & (33)..35..38 \\ \hline
 SMP thr.      & SMP lat. (*)  &1.08&3.39&32.44&32.44& (31)..35..37 \\ \hline
\multicolumn{2}{|c|}{SMP lat.} &1.12&2.32&21.92&21.92& -            \\ \hline
\multicolumn{2}{|c|}{SMP lat./thr.} &1.28&9.01&97.40&97.40& 59..64       \\ \hline
\multicolumn{2}{|c|}{SMP thr.} &   - &     - &    - &     - & (58)..59..65 \\ \hline
\end{tabular}
\end{center}
\caption{Results, 20000 interrupts or cca 6 minutes, latencies in
  \usec. (*) SMP affinity, as described in the previous chapter, was
  used to separate tasks on different cores within SMP Linux.}
\end{table*}

In the table 1, trends are clear: the bare-metal application has
latencies below 2\usec{} in all the cases. The Linux latencies depend on
the workload and the UP/SMP configuration, but the maximum Linux latencies are
above 8\usec{} in all the cases. When the system is loaded by the mtbench,
the maximum latency is above 30\usec, more than $10\times$ worst latency
of the bare-metal application. That shows that AMP is suitable for keeping
interrupt latency low.

\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}
\renewcommand{\textfraction}{0.2}
\renewcommand{\floatpagefraction}{0.8}

%\begin{figure*}[h]
%  \caption{Both axis are at linear scale, x scale shows latency in \usec,
%  y scale shows relative frequency of such latency. This shows how big the latency differences
%  are between various configurations.}
%  \centering
%  \includegraphics[width=1.1\textwidth,natwidth=800,natheight=1000]{linear.png}
%\end{figure*}

%\begin{figure*}[h]
%  \caption{Both axis are at logarithmic scale, x scale shows latency in \usec,
%  y scale shows relative frequency of such latency. This figure contains the same data
%  as the previous one.}
%  \includegraphics[width=1.1\textwidth,natwidth=800,natheight=1000]{logscale.png}
%\end{figure*}

\begin{figure*}[h]
  \caption{Both axis are in linear scale, x scale shows latency in
    \usec, y scale shows relative frequency of such
    latency. The AMP configurations are shown here. The rightmost dot in these graphs is from the
    first, cache-cold measurement. Data below 0.80 \usec{} are summarized as a single value due to
    statistics code limitations.}
  \centering
  \includegraphics[width=1.1\textwidth,natwidth=800,natheight=1000]{big_amp.png}
\end{figure*}

\begin{figure*}[h]
  \caption{Both axis are in linear scale, x scale shows latency in
    \usec, y scale shows relative frequency of such
    latency. UP and SMP configurations are shown here. Notice the different scale of the x axis.}
  \centering
  \includegraphics[width=1.1\textwidth,natwidth=800,natheight=1000]{big_linux.png}
\end{figure*}

\clearpage

\subsection {Latency task characteristics}

The results also show that the ``latency'' task does not have any
measurable impact on the ``throughput'' task. That was expected -- the
latency task is rather simple, and the interrupt load (cca 50 per second) is not high.

Running mtbench on only one of the two cores causes
severe performance penalty. In the tests, single core provides cca
58\% of the performance of both cores. It is possible that running Linux
in AMP mode (as opposed to UP) causes a small additional performance
impact (for example due to reduction in the cache allocated to Linux). More testing would be needed to confirm that this is the case, these tests show that the performance impact is around 10\%.

\maxten{} latency shows, that the bare-metal
application with polling provides the lowest \maxten{} latency -- 0.80
\usec. Switching to interrupts raises the minimum latency significantly,
but the \maxten latency only rises to 0.84 \usec.  It is likely that the system
caches prevent spinning CPU from having an impact on second core, and
simpler configurations without interrupts can be used, without a
performance penalty on the other core.

\subsection {Linux behaviour}

Linux is not a real-time operating system, so it exhibits a ``long
tail'' behaviour, with latencies occasionally jumping to many times the
average latency. The throughput task does have a significant impact on
both average and maximum latencies.

Binding of the latency task to an unused CPU using CPU affinity does
reduce the average latency significantly, but does not affect the
maximum latency much.

\subsection {AMP impact on real-time operating system}

Running
bare-metal application in AMP mode with idle Linux on the other one
has a clear impact on the \maxten{} latency: it rises approximately 15\% in
polling mode and 40\% in interrupt mode. Linux in AMP mode
with mtbench running raises the latency to approximately $2\times$ the
original value. mtbench is not the worst case here, 
a quick test with ``find /'' over NFS slowed the bare-metal
application on the other core slightly more. But even in the worst
case, the \maxten{} latency is only 20\% of the latency of the idle UP Linux, and
1.8\% of the latency of the SMP Linux running mtbench.

Similar slowdowns are expected if a real-time operating system
is used instead of bare-metal application: maximum latencies will
rise by $2\times$ to $3\times$ according to these tests.




\section {Other configuration possibilities and \\ their trade-offs}

% List configuration possibilities not benchmarked. This should include
% real-time Linux extensions, Xenomai, real-time tasks implemented
% directly in FPGA netlist and running in NIOS softcores.  All briefly
% mentioned with some links for further informations, without any tests
% done.

The previous chapters explored possibilities offered by using
multiple CPU cores in the system. There are opther options, shown here.

\subsection {Implementing real-time functionality in hardware}
\label{sec:fpga}

Implementing the required real-time functionality directly in hardware
will certainly have the best latency and reliability characteristics,
but the development cost will be high, and unless the hardware is produced in
very high numbers, per-unit cost will be high, too.

Systems with FPGAs offer the option to implement the real-time functionality directly in a FPGA. This will make the development slightly easier, and per-unit costs
will be reasonable even for small series. Care will be needed to lock
down the FPGA, so that the CPU can not reprogram it or interfere with its
operation.

\subsection {Implementing real-time functionality in a simple additional CPU}

If the real-time functionality is too complex for a direct hardware
implementation, adding an additional CPU may be an option. The
reliability will be very good if its subsystems can be completely
separated.  It will make the development cost somewhat
higher. Per-unit costs will be somewhat higher due to additional
chips, but custom chips will not be required making this an option for
small series. A simple CPU might also be easier to certify, and it
should be much easier to protect it from single event upsets. 

% Some SoCs, like i.MX6SX \cite{mx6sx} already offer such additional simple cores on the SoC.

The FPGA also adds a possibility to do this without additional hardware: soft CPU cores such as NIOS II \cite{nios}
% , Pico/MicroBlaze or AEMB
can be synthesized
in the FPGA (NIOS II costing as low as 600 logic elements units on Cyclone
V), and used to run a separate application \cite{device-amp}. Care will be needed to lock
down the FPGA and lock down the memory resources needed by the soft core
instance. The development cost will be similar to the hardware approach,
per-unit costs will be close to zero, if such a FPGA is already present in the
project. Of course, soft CPU cores will be order of magnitude slower than high-performance cores.

\subsection {Hypervisors}

It is possible to separate the real-time and non-real-time tasks using a
simple operating system, usually called ``hypervisor''. The resulting system
is slower and more complex, but with the correct design a maximum interrupt
latency can be guaranteed. Examples of such approaches are L4
micro-kernel \cite{l4}, Xenomai \cite{xenomai}, RTLinux \cite{rtlinux}, RTAI \cite{rtai},
PIKEOS (non-free) \cite{pikeos}.

\subsection {Real-time operating systems}

A real-time operating system is going to offer similar performance
trade-offs to the hypervisor approach: it is going to have slightly lower
throughput and slightly higher average latency than non-real-time
systems, but will guarantee a maximum latency.

Linux with PREEMPT\_RT patches \cite{preempt} may be an interesting
choice here, because it offers a familiar programming interface and GPL
licensing. Due to the high complexity of the Linux kernel, this approach is
not likely to be useful for certification projects.

According to ``How fast is fast enough?'' paper
\cite{xenomai-preempt-paper}, PREEMPT\_RT slows down median interrupt
latency about $4\times$, and worst-case interrupt latency is about
$85\times$ the median one. Xenomai has similar median interrupt
latency to stock Linux (20\% slower), and maximum interrupt latency is
slightly more cca $4.1\times$ of the median. In these tests, AMP
configurations have lower interrupt latencies than the unmodified Linux,
and the maximum interrupt latency is less than $4\times$ the minimum
one. (Which should not be surprising: there is a special, bare-metal
operating system, with a dedicated CPU core running it.)

\section {Conclusions}

Previous chapters have shown that a bare-metal application running on one CPU and Linux
on another CPU in AMP configuration can provide significantly lower
latencies for interrupt handling than stock Linux, while providing
a familiar programming interface for non-real-time parts. When one CPU
core becomes unavailable for Linux tasks, the performance hit is
significant. AMP has some non-technical advantages (ability to use
existing code for existing real-time operating system, less code to
certify) and also some disadvantages (two operating systems to
compile, maintain and debug).

Whether AMP is the best fitting solution for a project depends on many
details, including performance requirements, complexity of the overall
system, complexity of the real-time parts, certification requirements
and availability of free space in the FPGA.

\medskip

\begin{thebibliography}{9}

\bibitem{board}
  Altera Cyclone V SoC Board,
  \url{http://rocketboards.org/foswiki/view/Documentation/AlteraSoCDevelopmentBoard}

\bibitem{linux}
  Linux, \url{https://www.kernel.org/}

\bibitem{freebsd}
  FreeBSD, \url{https://www.freebsd.org/}
  
\bibitem{minix}
  Minix, \url{http://www.minix3.org/}
  
\bibitem{u-boot}
  U-Boot, \url{http://www.denx.de/wiki/U-Boot/}

\bibitem{ucos}
  Micrium \ucos, \url{http://micrium.com/rtos/ucosii/overview/}

\bibitem{l4}
  The L4 $\mu$-kernel family, \url{http://os.inf.tu-dresden.de/L4/}

\bibitem{trustzone}
  TrustZone, \url{http://www.arm.com/products/processors/technologies/trustzone/index.php}

\bibitem{xenomai}
  Xenomai, \url{http://xenomai.org/api-reference/}

\bibitem{xenomai-preempt-paper} Dr. Jeremy H. Brown, Brad Martin: How
  fast is fast enough? Choosing between Xenomai and Linux for
  real-time applications. \url{https://www.osadl.org/fileadmin/dam/rtlws/12/Brown.pdf}

\bibitem{rtlinux}
  RTLinux, \url{https://en.wikipedia.org/wiki/RTLinux}

\bibitem{rtai}
  RTAI, \url{https://www.rtai.org/}

\bibitem{pikeos}
  PikeOS Hypervisor, 
  \url{https://www.sysgo.com/products/pikeos-rtos-and-virtualization-concept/}

\bibitem{preempt}
  PREEMPT\_RT, \url{https://rt.wiki.kernel.org/index.php/Main_Page}

\bibitem{nios}
  \url{http://wl.altera.com/devices/processor/nios2/ni2-index.html}

\bibitem{amp-linux}
  \url{https://gitlab.denx.de/amp/linux}

\bibitem{amp-u-boot}
  \url{https://gitlab.denx.de/amp/u-boot}
  
\bibitem{amp-sparrow}
  \url{https://gitlab.denx.de/amp/sparrow}

% \bibitem{mx6sx}
  %  \url{http://www.digikey.com/catalog/en/partgroup/i-mx6sx/52124}

\bibitem{device-amp}
  \url{https://rocketboards.org/foswiki/view/Projects/DeviceWideAMP}
  
\end{thebibliography}

Distribute under Creative commons
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license
(\url{https://creativecommons.org/licenses/by-sa/4.0/}).


\end{document}

pdflatex paper.tex && evince paper.pdf
M-x whizzytex-mode

